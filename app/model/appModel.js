'user strict';
var sql = require('./db.js');

//Task object constructor
var Equipment = function(equipment){
    //this.task = 'task.task';
    this.equipement_id = equipment.equipement_id;
    this.address = equipment.address;
    this.status = equipment.status;
    this.contract_start_date = new Date();
    this.contract_end_date = new Date();
};
Equipment.createEquipment = function createEquipment(newEquipment, result) {
        sql.query("INSERT INTO tasks set ?", newEquipment, function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    console.log(res.insertId);
                    result(null, res.insertId);
                }
            });
};
Equipment.getEqipmentById = function getEqipmentById(equipmentId, result) {
        sql.query("Select equipement_id, address, status, contract_start_date as contract_start, contract_end_date as contract_end from tasks where equipement_id = ? ", equipmentId, function (err, res) {
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    result(null, res);

                }
            });
};
Equipment.getAllEquipment = function getAllEquipment(result) {
        sql.query("Select * from tasks", function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    result(null, err);
                }
                else{
                  console.log('tasks : ', res);

                 result(null, res);
                }
            });
};

Equipment.getNumberOfEqpuipment = function getNumberOfEqpuipment(noOfData, result) {
        sql.query("Select * from tasks limit  "+noOfData, function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    result(null, err);
                }
                else{
                  console.log('tasks : ', res);

                 result(null, res);
                }
            });
};

Equipment.updateById = function(id, equipment, result){
  sql.query("UPDATE tasks SET address = ? , status = ? WHERE equipement_id = ?", [equipment.address, equipment.status, id], function (err, res) {
          if(err) {
              console.log("error: ", err);
                result(null, err);
             }
           else{
             result(null, res);
                }
            });
};
Equipment.remove = function(id, result){
     sql.query("DELETE FROM tasks WHERE equipement_id = ?", [id], function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    result(null, err);
                }
                else{

                 result(null, res);
                }
            });
};

module.exports= Equipment;
