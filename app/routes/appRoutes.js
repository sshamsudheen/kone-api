'use strict';
module.exports = function(app) {
  var todoList = require('../controller/appController');

  // todoList Routes
  app.route('/kone-api/equipments')
    .get(todoList.list_all_equipments)
    .post(todoList.create_a_equipment);

  app.route('/kone-api/equipments/search/:tasks') // displau tasks no of equpments
   .get(todoList.show_tasks);

   app.route('/kone-api/equipments/:taskId')
    .get(todoList.read_a_equipment)
    .put(todoList.update_a_equipment)
    .delete(todoList.delete_a_equipment);
    };
