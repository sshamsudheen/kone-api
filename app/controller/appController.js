'use strict';

var Equipment = require('../model/appModel.js');

exports.list_all_equipments = function(req, res) {
  Equipment.getAllEquipment(function(err, task) {

    console.log('controller')
    if (err)
      res.send(err);
      console.log('res', task);
    res.send(task);
  });
};


exports.show_tasks = function(req, res) {
  Equipment.getNumberOfEqpuipment(req.params.tasks, function(err, task) {
    //res.status(400).send({ error:true, message: req.params.tasks });
    console.log('controller')
    if (err)
      res.send(err);
      console.log('res', task);
    res.send(task);
  });
};


exports.create_a_equipment = function(req, res) {
  var new_task = new Equipment(req.body);

  //handles null error
   if(!new_task.equipement_id || !new_task.status || !new_task.address){

            res.status(400).send({ error:true, message: 'Please provide Equipement_id / Address / status' });

        }
else{

  Equipment.createEquipment(new_task, function(err, task) {

    if (err)
      res.send(err);
    res.json(task);
  });
}
};


exports.read_a_equipment = function(req, res) {
  Equipment.getEqipmentById(req.params.taskId, function(err, task) {
    if (err)
      res.send(err);
    res.json(task);
  });
};


exports.update_a_equipment = function(req, res) {
  Equipment.updateById(req.params.taskId, new Equipment(req.body), function(err, task) {
    if (err)
      res.send(err);
    res.json(task);
  });
};


exports.delete_a_equipment = function(req, res) {


  Equipment.remove( req.params.taskId, function(err, task) {
    if (err)
      res.send(err);
    res.json({ message: 'Task successfully deleted' });
  });
};
