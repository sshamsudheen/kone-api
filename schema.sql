CREATE TABLE IF NOT EXISTS equipments (
    id INT PRIMARY KEY AUTO_INCREMENT,
    equipement_id INT NOT NULL,
    address VARCHAR(255) NOT NULL,
    status ENUM('Running', 'Stopped') DEFAULT 'Stopped',
    contract_start_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    contract_end_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    UNIQUE (equipement_id )
);

insert into equipments (equipement_id, address) values (12345,'sasasas');

ALTER TABLE tasks
ADD equipement_id INT NOT NULL;

ALTER TABLE tasks ADD address VARCHAR(255) NOT NULL


CREATE TABLE IF NOT EXISTS `tasks` (
  id INT PRIMARY KEY AUTO_INCREMENT,
  equipement_id INT NOT NULL,
  address VARCHAR(255) NOT NULL,
  status ENUM('Running', 'Stopped') DEFAULT 'Stopped',
  contract_start_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  contract_end_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  UNIQUE (equipement_id )
);

ALTER TABLE `tasks` ADD PRIMARY KEY (`id`);
ALTER TABLE `tasks` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;


INSERT INTO `tasks` (`id`, `equipement_id`, `address`, `status`) VALUES
(1, 123, 'espoo', 'Running'),
(2, 456, 'helsinki', 'Stopped' ),
(3, 789, 'vantaa', 'Running' ),
(4, 987, 'espoo', 'Running'),
(5, 654, 'espoo', 'Stopped');
